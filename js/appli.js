$( document ).ready(function() {

    let selectedGenre, game, i, id, details;
    
    
        $("select.genre").change(function(){
            selectedGenre = $(this).children("option:selected").val();
            game = "";
            details = "";
            $("#zone_game").html(game);
            $("#zone_details").html(details);
            $("#image").remove();
            $("#image2").remove();
        });


    let getGames = function(data) {
        console.log( "données api", data)
        i = Math.floor(Math.random()*20+1);
        game = data.results[i].name;
        $("#zone_game").html(game);
        id = data.results[i].id;
        console.log(id);
    }

    let getDetails = function(data) {
        console.log( "données api détails", data)
        details = data.description;
        $("#zone_details").html(details);
        let image = data.background_image;
        let image2 = data.background_image_additional;
        $("#zone_image").append('<div id="image"><img src="'+image+ '"/></div><div id="image2"><img src="'+image2+ '"/></div>');
    }


    $("#button1").click(function() {
        game = "";
        details = "";
        $("#zone_game").html(game);
        $("#zone_details").html(details);
        $("#image").remove();
        $("#image2").remove();
        let url1 = "https://api.rawg.io/api/games?" + selectedGenre;
        $.get(url1, getGames).done(function() {
        })
        .fail(function() {
            alert( "error");
        })
        .always(function() {
        });
    });
    $("#button2").click(function() {
        let url2 = "https://api.rawg.io/api/games/" + id;
        $.get(url2, getDetails).done(function() {
        })
        .fail(function() {
            alert( "error");
        })
        .always(function() {
        });
        });

    });
